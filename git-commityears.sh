#!/bin/sh

# ./git-commityears.sh AUTHOR FILE [...]
# for each FILE; prints a line FILE: YEARS,
# where YEARS is a compacted human-readable list of years where AUTHOR
# (regular expression) has commits recorded in Git; years is the commit date
# (as year where things were published).

# (C) Copyright 2009 Matthias Andree, GNU GPL v3.

years() {
  git log -C -C --pretty='tformat:%ci' --author="$2" -- "$1" \
  | cut -f1 -d- \
  | ./crunchunsigned
}

author="$1"
shift
while [ "x$1" != x ] ; do
	echo "$1: $(years "$1" "$author")"
	shift
done
