/* quickmkdir - creates message.id/000 ... /999 directories for leafnode
 *
 * (C) Copyright 2001 by Matthias Andree <matthias.andree@gmx.de>
 *  Modified and copyright of the modifications 2002 by Ralf Wildenhues
 * <ralf.wildenhues@gmx.de>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 or 2.1 of
 * the License. See the file COPYING.LGPL for details.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "leafnode.h"
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pwd.h>
#include <grp.h>

static void
die(const char *tag)
{
    perror(tag);
    exit(1);
}

int
main(int argc, char **argv)
{
    uid_t user, myuid;
    gid_t group;
    struct passwd *pw;
    struct group *gr;
    char blubb[1024]; /* RATS: ignore */
    char *blubb2;
    const char *prepend;
    int i, maxlen;

    if (!(pw = getpwnam(NEWS_USER))) {
	fprintf(stderr, "getpwnam(\"%s\"): ", NEWS_USER);
	die(NULL);
    }
    user = pw->pw_uid;
    if (!(gr = getgrnam(NEWS_GROUP))) {
	fprintf(stderr, "getpwnam(\"%s\"): ", NEWS_GROUP);
	die(NULL);
    }
    group = gr->gr_gid;
    myuid = getuid();

    prepend = "";
    if (argc == 2 && argv[1] && argv[1][0])
	prepend = argv[1];
    if (argc > 2)
	exit(1);

    if (strlen(prepend) + 100 > sizeof(blubb)) {
	fputs("argument too long\n", stderr);
	exit(1);
    }

    strcpy(blubb, prepend);	/* RATS: ignore */
    blubb2 = blubb + strlen(blubb);
    maxlen = sizeof(blubb) - strlen(blubb) - 20;

    (void)umask(02);
    for (i = 0; i < 1000; i++) {
	sprintf(blubb2, "%-.*s/message.id/%03d", maxlen, spooldir, i);
	if (mkdir(blubb, 02775)) {
	    if (errno != EEXIST)
		die("mkdir");
	}
	if (!myuid) {
	    if (chown(blubb, user, group))
		die("chown");
	}
    }
    exit(0);
}
