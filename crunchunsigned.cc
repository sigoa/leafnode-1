// crunchunsigned.cc
// short program to sort, unique, and compress a set of whitespace-separated
// unsigned numbers read from stdin and print them to stdout.
//
// in leafnode's scope only intended as maintainer tool and aid to update
// copyright statements on files (in collaboration with git-commityears.sh),
// not required for developers or end users
//
// (C) Copyright 2009 Matthias Andree, GNU GPL v3.

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	vector<unsigned> work;
	unsigned i, count;

	// read, sort and unique data
	while (cin >> i) {
		work.push_back(i);
	}
	sort(work.begin(), work.end());
	work.erase(unique(work.begin(), work.end()), work.end());
	
	i = 0;

	count = work.size();
	while(i < count) {
		if (i + 2 < count
		    && work[i] + 1 == work[i + 1]
		    && work[i] + 2 == work[i + 2]) {
			int j = i + 1;
			while(work[j] + 1 == work[j+1]) j ++;
			cout << work[i] << " - " << work[j];
			i = j + 1;
		} else {
			cout << work[i];
			++i;
		}
		if (i < count) cout << ", ";
	}
	cout << "\n";
	return 0;
}
