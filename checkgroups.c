/*
 * Checkgroups: script that updates the description of newsgroups
 * Input file : a checkgroups script that contains the name of
 *              the newsgroup and its description in one line
 *
 * Written and copyrighted by Cornelius Krasel, April 1997
 * Source code borrows a lot from fetch(1).
 *
 * See file COPYING for restrictions on the use of this software.
 */

#include "leafnode.h"
#include "ln_log.h"

#include <sys/types.h>
#include <ctype.h>
#include "system.h"
#include <fcntl.h>
#include <stdlib.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/resource.h>
#include <unistd.h>

int debug = 0;
int verbose = 0;

static void
process_input(char *s)
{
    FILE *f;
    char *l;
    static time_t now;

    if (!now) now = time(NULL);

    f = fopen(s, "r");
    if (!f) {
	fprintf(stderr, "cannot open %s: %s\n", s, strerror(errno));
	return;
    }

    while ((l = getaline(f))) {
	char *p;
	struct newsgroup *g;

	p = l;
	if (isalnum((unsigned char)*p)) {
	    while (*p && !isspace((unsigned char)*p))
		p++;
	    if (*p)
		*p++ = '\0';
	    if ((g = findgroup(l)) != NULL) {
		fprintf(stderr, "%s\n", l);
		if (strlen(p) > 0) {
		    if (g->desc)
			free(g->desc);
		    g->desc = critstrdup(p, "process_input");
		}
	    } else {
		fprintf(stderr, "%s NEW\n", l);
		insertgroup(l, 1, 0, now);
		if (*p)
		    newgroupdesc(l, p);
	    }
	}
    }

    fclose(f);
    mergegroups();
}

int
main(int argc, char *argv[])
{
    int fd;
    myopenlog("checkgroups");

    fd = open(".", O_RDONLY);
    if (fd < 0) {
	perror("checkgroups: cannot store cwd");
	exit(EXIT_FAILURE);
    }

    if (!initvars(argv[0]))
	exit(EXIT_FAILURE);

    if (fchdir(fd)) {
	perror("checkgroups: cannot restore cwd");
	exit(EXIT_FAILURE);
    }
    (void)close(fd);

    if (argc != 2) {
	fprintf(stderr, "Usage: %s checkgroupsfile\n", argv[0]);
	exit(1);
    }

    umask(2);

    if (readconfig(0)) {
	fprintf(stderr, "cannot read configuration.\n");
	exit(EXIT_FAILURE);
    }

    if (try_lock(timeout_lock)) {
	fprintf(stderr, "could not obtain lock.\n");
	exit(EXIT_FAILURE);
    }

    readactive();		/* read groupinfo file */
    if (!active)
	fakeactive();		/* make sure we have up to date water marks for existing groups */
    process_input(argv[1]);
    if (writeactive()) ln_log(LNLOG_SERR, LNLOG_CTOP, "Error writing groupinfo.");
    unlink(lockfile);
    freeactive(active);
    freegetaline();
    exit(0);
}
