leafnode-SA-2003:01.fetchnews-hang

Topic:		potential denial of service in leafnode

Announcement:	leafnode-SA-2003:01
Writer:		Matthias Andree
Version:	1.01
Announced:	2003-09-02
Category:	main
Type:		potential denial of service
Impact:		fetchnews hangs, no new fetchnews/texpire processes
		can be started
Credits:	Joshua Crawford (for sending a precise bug report)
Danger:		medium:
		- only one process will clog memory since leafnode-1.9.20
		  bug can hang for an extended amount of time
		- no privilege escalation through this bug
CVE ID:		CVE-2003-0744
URL:		http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2003-0744

Affects:	leafnode 1.9.3 (1999) up to 1.9.41 (2003)

Not affected:	leafnode 1.9.42 and newer

Default install: affected.

Introduced:	between 1999-03-03 and 1999-07-15 (no precise date found)
		1999-07-15 07:49    leafnode 1.9.3 announced by Cornelius Krasel

Corrected:	2003-06-20 22:57:48 UTC (CVS) - committed corrected version
		2003-06-27 11:29    leafnode 1.9.42 released

0. Release history

2003-09-02	1.00 initial announcement
2005-05-07	1.01 add CVE URL

1. Background

leafnode is a store-and-forward proxy for Usenet news, is uses the
network news transfer protocol (NNTP). It consists of several
collaborating programs, the server part is usually started by inetd,
xinetd or tcpserver, the client part is usually started by cron or
manually.

This security announcement pertains to leafnode-1, the stable branch.

The leafnode-2 development branch has not yet seen a stable release, so
it is not subject to security announcements.

2. Problem description

A vulnerability was found in the fetchnews program (the NNTP client) that
may under some circumstances cause wait for input that never arrives,
fetchnews "hangs". This hang does not cost CPU.

This bug was not deemed security relevant at first, but as it can
be triggered from the outside, by providing malformatted (non-RFC-1036)
Usenet news articles, and because it then stops unattended systems from
functioning, it was decided to release this security announcement.

3. Impact

As only one fetchnews program can run at a time, subsequently started
fetchnews and texpire programs will terminate immediately. This means
that the news base will no longer be updated, older articles will no
longer expire, until the hanging fetchnews process gets unstuck, usually
through a manual "kill" command or a reboot.

4. Workaround

No reliable workaround possible.

NOTE: Killing fetchnews before completion leaves stale data on disk and
is therefore not deemed reliable, although it relieves the immediate
"cannot start texpire or fetchnews" condition.

5. Solution

Upgrade your leafnode package to version 1.9.42 or later.

Note that leafnode 1.9.X versions are deemed stable, and it is usually
best to go for the latest released 1.9.X version to have all the other
bug fixes as well. No broken-out version of this patch will be
provided, distributors are urged to update to the latest leafnode
version.

leafnode 1.9.42 is available from sourceforge:

http://sourceforge.net/project/showfiles.php?group_id=57767&release_id=168122

This policy of not providing a broken-out patch may generate a conflict
with some distribution's post-release update policies.

As the current leafnode maintainer, I do not have financial and time
ressources to provide support for any but the latest released version.

People keep reporting bugs about leafnode-1.9.33, 1.9.24 or 1.9.19,
which is a waste of time for the user and the leafnode maintainer.

6. Solution details

revision 1.111
date: 2003/06/20 22:57:48;  author: emma;  state: Exp;  lines: +10 -4

A. References

leafnode home page: http://www.leafnode.org/
