leafnode-SA-2004:01.fetchnews-hang-no-body

Topic:		potential denial of service in leafnode

Announcement:	leafnode-SA-2004:01
Writer:		Matthias Andree
Version:	1.01
Announced:	2004-01-09
Category:	main
Type:		potential denial of service
Impact:		fetchnews hangs, no new fetchnews/texpire processes
		can be started
Credits:	Toni Viemer�
Danger:		medium:
		- only one process will clog memory since leafnode-1.9.20
		  bug can hang for an extended amount of time
		- no privilege escalation through this bug
CVE Name:	CVE-2004-2068
URL:		http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2004-2068

Affects:	leafnode versions up to and including 1.9.47 (2004)

Not affected:	leafnode 1.9.48

Default install: affected.

Corrected:	2004-01-09 00:53 UTC (CVS) - committed corrected version
		2004-01-09 01:26             leafnode 1.9.48 released

0. Release history

2004-01-09	1.00 initial announcement
2005-05-07	1.01 add CVE name and URL

1. Background

leafnode is a store-and-forward proxy for Usenet news, is uses the
network news transfer protocol (NNTP). It consists of several
collaborating programs, the server part is usually started by inetd,
xinetd or tcpserver, the client part is usually started by cron or
manually.

This security announcement pertains to leafnode-1, the stable branch.

The leafnode-2 development branch has not yet seen a stable release, so
it is not subject to security announcements.

2. Problem description

A vulnerability was found in the fetchnews program (the NNTP client) that
may under some circumstances cause a wait for input that never arrives,
fetchnews "hangs". This hang does not cost CPU.

3. Impact

As only one fetchnews program can run at a time, subsequently started
fetchnews and texpire programs will terminate immediately. This means
that the news base will no longer be updated, older articles will no
longer expire, until the hanging fetchnews process gets unstuck, usually
through a manual "kill" command or a reboot.

4. Workaround

Set minlines=1 in your configuration file, usually /etc/leafnode/config.
This workaround will only work with leafnode 1.9.47, not with older
versions.

NOTE: Killing fetchnews before completion leaves stale data on disk and
is therefore not deemed reliable, although it relieves the immediate
"cannot start texpire or fetchnews" condition.

5. Solution

Upgrade your leafnode package to version 1.9.48.

Note that leafnode 1.9.X versions are deemed stable, and it is usually
best to go for the latest released 1.9.X version to have all the other
bug fixes as well. No broken-out version of this patch will be
provided, distributors are urged to update to the latest leafnode
version. The diff between leafnode 1.9.47 and 1.9.48 may serve as a
replacement, provided it applies to the version in question. It may very
well not.

leafnode 1.9.48 is available from sourceforge:

http://sourceforge.net/project/showfiles.php?group_id=57767&release_id=208614

A. References

leafnode home page: http://leafnode.sourceforge.net/
