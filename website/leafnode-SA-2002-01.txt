leafnode-SA-2002:01.versions

Topic:		vulnerabilities in leafnode

Announcement:	leafnode-SA-2002:01
Writer:		Matthias Andree
Version:	1.01
Announced:	2002-12-29
Category:	main
Type:		denial of service
Impact:		CPU busy loop
Credits:	Jan Knutar (jknutar, nic dot fi), for finding the bug
		Mark Brown (broonie, debian dot org), for pointing out DoS
		capability
Danger:		medium (only trusted users should be able to connect to
		leafnode, lest it was installed improperly).
CVE Name:	CVE-2002-1661
URL:		http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2002-1661

Affects:	leafnode 1.9.20 up to 1.9.29

Not affected:	leafnode 1.9.30 and 1.9.31

Default install: unaffected.

Introduced:	2002-03-14 23:41:40 UTC (CVS)
		2002-03-25 20:58    leafnode 1.9.20 released

Corrected:	2002-11-08 17:14:41 UTC (CVS) - committed corrected version
		2002-12-04 00:40    leafnode 1.9.30 released

0. Release history

2002-12-29	1.00 initial announcement
2005-05-07	1.01 add CVE name and URL

1. Background

leafnode is a store-and-forward proxy for Usenet news, is uses the
network news transfer protocol (NNTP). It consists of several
collaborating programs, the server part is usually started by inetd,
xinetd or tcpserver, the client part is usually started by cron or
manually.

This security announcement pertains to leafnode-1, the stable branch.

The leafnode-2 development branch has not yet seen a stable release, so
it is not subject to security announcements.

2. Problem description

A vulnerability was found in the leafnode program (the NNTP server) that
may go into an infinite loop with 100% CPU use when an article that has
been crossposted to several groups, one of which is the prefix of
another, and when this article is then requested by its Message-ID.

Note though that one newsgroup name MUST NOT be the prefix of anohter
newsgroup's name, these problems show up however in badly-maintained or
anarchistic hierarchies such as alt.* or free.*.

3. Impact

This vulnerability can make leafnode's nntpd server, named leafnode, go
into an unterminated loop when a particular article is requested. The
connection becomes irresponsive, and the server hogs the CPU. The client
will have to terminate the connection and connect again, and may fall
prey to the same problem; ultimately, there may be so many leafnode
processes hogging the CPU that no serious work is possible any more and
the super user has to kill all running leafnode processes.

4. Workaround

No sane workaround can be presented.

5. Solution

Upgrade your leafnode package to version 1.9.30 or 1.9.31, or apply the
patch below and recompile and reinstall. Note that leafnode 1.9.X
versions are stable, and it is usually best to go for the latest
released 1.9.X version to have all the other bug fixes as well.

Note that while leafnode 1.9.19 is unaffected, it has other critical
bugs, it can corrupt parts of its news spool under certain circumstances
and should not be used. The details are however not subject of this
security announcement as these problems are believed not to be security
problems.

leafnode 1.9.31 is available from sourceforge:

http://sourceforge.net/project/showfiles.php?group_id=57767&release_id=130347

6. Solution details

revision 1.83
date: 2002/11/08 17:14:41;  author: emma;  state: Exp;  lines: +1 -1

A. References

leafnode home page: http://www.leafnode.org/

B. Patch

diff -u -C4 -r1.81 -r1.83
*** nntpd.c	24 Sep 2002 16:04:01 -0000	1.81
--- nntpd.c	8 Nov 2002 17:14:41 -0000	1.83
***************
*** 520,527 ****
--- 520,528 ----
  			localartno = strtoul(q, NULL, 10);
  			markgroup = group->name;
  			break;
  		    }
+ 		    p = q;
  		}
  	    }
  	    /* if we don't have a localartno, then we need to mark this
  	     * article in a different news group */
