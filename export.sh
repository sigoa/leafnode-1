#! /bin/sh

owndir="$HOME"/public_html/leafnode/leafnode-1.git/

a=0
echo "===>  Pushing to own home directory"
printf "branches: " ; git push --all  "$owndir" || a=1
printf "tags:     " ; git push --tags "$owndir" || a=1
echo "===>  Repacking"
( cd "$owndir" && git repack --max-pack-size=1 --window=250 --depth=250 -d )
echo "===>  Finished."
if [ $a -eq 0 ] ; then
  echo "Success."
else
  echo "Error, see above for details!"
fi
exit $a
