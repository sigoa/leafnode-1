/*
    getaline - fetch a single line from a stdio stream, arbitrary length
    Copyright (C) 2000 - 2002 Matthias Andree

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    As special permission, when linked against the leafnode package,
    leafnode programs themselves need not be put under the GPL.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef GETLINE_H
#define GETLINE_H

#include <sys/types.h>
#include "config.h"

/* from getline.c */
#ifndef HAVE_GETLINE
ssize_t getline(char **, size_t *, FILE *);	/* fgets replacement */
#endif
ssize_t _getline(char *to, size_t size, FILE * stream);

#endif
