#! /bin/bash

set -e
cwd="$(pwd)"
#rpmdir=/usr/src/packages
dest=$HOME/public_html/leafnode/
tmp=/var/tmp
vers=`perl -n -l -e 'if (/AC_INIT\(.+,\[(.+)\]\)/) { print "$1\n"; last; }' configure.ac`
over=`echo $vers | perl -e 'my @a = split /\./, <>; $a[2]--; print join(".", @a), "\n";'`
if [ "$OLDVER" ] ; then over="$OLDVER" ; fi
vers_s=`echo $vers | sed 's/.rel//'`
over_s=`echo $over | sed 's/.rel//'`
echo "$vers <- $over"
rm -fr $tmp/leafnode-$over
rm -fr $tmp/leafnode-$vers
tar -C $tmp -xzf $dest/leafnode-$over.tar.gz
tar -C $tmp -xzf $dest/leafnode-$vers.tar.gz
(
    cd $tmp && diff -Nur leafnode-$over leafnode-$vers \
    | gzip -9v >$dest/upgrade-$over_s-to-$vers_s.diff.gz
    gzip -cd $dest/upgrade-$over_s-to-$vers_s.diff.gz \
    | diffstat >$dest/diffstat-$vers_s.txt
)
(
    cd $dest
    test -f MD5 || gpg MD5.asc
    test -f SHA1 || gpg SHA1.asc
    echo >>MD5
    md5sum -b leafnode-$vers.tar.{bz2,gz,xz} \
    upgrade-$over_s-to-$vers_s.diff.gz >>MD5
    echo >>SHA1
    sha1sum -b leafnode-$vers.tar.{bz2,gz,xz} \
    upgrade-$over_s-to-$vers_s.diff.gz >>SHA1
    rm MD5.asc && gpg --clearsign MD5
    rm SHA1.asc && gpg --clearsign SHA1
    gpg -ba --sign upgrade-$over_s-to-$vers_s.diff.gz
    #tar -Oxjf leafnode-$vers.tar.bz2 leafnode-$vers/leafnode.spec >"$rpmdir"/SPECS/leafnode.spec
    #ln -sfn $dest/leafnode-$vers.tar.bz2 "$rpmdir"/SOURCES/
    #rpmbuild -bb --sign --target=i486 "$rpmdir"/SPECS/leafnode.spec
    #mv "$rpmdir"/RPMS/i486/leafnode-$vers-1.i486.rpm .
    #mv "$rpmdir"/RPMS/i486/leafnode-debuginfo-$vers-1.i486.rpm .
    #mv "$rpmdir"/RPMS/i486/leafnode-debugsource-$vers-1.i486.rpm .
    cat <<EOT - | sftp -b - m-a@frs.sourceforge.net
	cd /home/frs/project/l/le/leafnode/leafnode
	mkdir $vers_s
	cd $vers_s
	put upgrade-$over_s-to-$vers_s.diff.gz
	put upgrade-$over_s-to-$vers_s.diff.gz.asc
EOT
	#put leafnode-$vers-1.i486.rpm
	#put leafnode-debuginfo-$vers-1.i486.rpm
	#put leafnode-debugsource-$vers-1.i486.rpm
)
