/* (C) 2002, 2009 by Matthias Andree
 *
 * This file is under the same license as the rest of leafnode. Please see the
 * file "COPYING" that should be in the same directory as this.
 */

#include "leafnode.h"

#include <stdio.h>
#include <string.h>

int main(int argc, char **argv) {
    static char env_path[] = "PATH=/bin:/usr/bin";

    /* DO NOT CHANGE THE OUTPUT FORMAT; EXTERNAL TOOLS DEPEND ON IT!
     * (namely, leafwa does)
     */
    fputs("version: leafnode-", stdout);
    puts(version);

    /* new in 1.11.7: -v mode to print more information, such as directories */
    if (argc > 1 &&
	    (0 == strcmp(argv[1], "-v")
	     || 0 == strcmp(argv[1], "--verbose")))
    {
	fputs("current machine: ", stdout);
	fflush(stdout);
	putenv(env_path);
	if (system("uname -a"))
	    puts(" (error)");
	fputs("sysconfdir: ", stdout);
	puts(sysconfdir);
	fputs("spooldir: ", stdout);
	puts(spooldir);
	fputs("lockfile: ", stdout);
	puts(lockfile);
#ifdef HAVE_IPV6
	puts("IPv6: yes");
#else
	puts("IPv6: no");
#endif
#ifdef HAVE_GETIFADDRS
	puts("HAVE_GETIFADDRS: yes");
#else
	puts("HAVE_GETIFADDRS: no");
#endif
	fputs("pcre version: ", stdout);
	puts(pcre_version());
    }
    return 0;
}
