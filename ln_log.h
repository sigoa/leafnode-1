#ifndef LNLOG_H
#define LNLOG_H

#include <stdarg.h>

/* severities */
/* in analogy to syslog levels, are passed on to syslog */
#if 0
#define	LNLOG_SEMERG	0	/*   system is unusable */
#define	LNLOG_SALERT	1	/*   action must be taken immediately */
#endif
#define	LNLOG_SCRIT	2	/*   critical conditions */
#define	LNLOG_SERR	3	/* * error conditions */
#define	LNLOG_SWARNING	4	/* * warning conditions */
#define	LNLOG_SNOTICE	5	/* * normal but significant condition */
#define	LNLOG_SINFO	6	/* * informational */
#define	LNLOG_SDEBUG	7	/* * debug-level messages */
#define LNLOG_SMIN      2	/* minimal used severity */
/* contexts */
/* define the context the log message occurs in
   think of it as "verbose level" */
#define LNLOG_CTOP        1	/* top level, always log */
#define LNLOG_CSERVER     2	/* server context */
#define LNLOG_CGROUP      3	/* group context */
#define LNLOG_CARTICLE    4	/* article context */
#define LNLOG_CALL        5	/* most verbose */

/* IMPORT */
extern int verbose;

/* EXPORT */
extern void ln_log_open(const char *ident);	/** open log, use \a ident as log tag */
extern void ln_log_use_console(int en);		/** allow logging to console depending on \a en */

/* log to stderr and syslog */
extern void ln_log(int severity, int context, const char *format, ...)
#ifdef __GNUC__
    __attribute__ ((format(printf, 3, 4)))
#endif
    ;

/* log to stdout and syslog */
extern void ln_log_so(int severity, int context, const char *format, ...)
#ifdef __GNUC__
    __attribute__ ((format(printf, 3, 4)))
#endif
    ;

/* log to stderr only */
extern void ln_log_prt(int severity, int context, const char *format, ...)
#ifdef __GNUC__
    __attribute__ ((format(printf, 3, 4)))
#endif
    ;

/* log to syslog only */
extern void ln_log_sys(int severity, int context, const char *format, ...)
#ifdef __GNUC__
    __attribute__ ((format(printf, 3, 4)))
#endif
    ;
#endif
