leafnode-SA-2005:01.fetchnews-crashes-on-timeout

Topic:		potential denial of service in leafnode

Announcement:	leafnode-SA-2005:01
Writer:		Matthias Andree
Version:	1.01
Announced:	2005-05-04
Category:	main
Type:		potential denial of service
Impact:		fetchnews crashes, some servers not queried
Danger:		low
		- malicious upstream server can easily be unlisted
CVE Name:	CVE-2005-1453
URL:		http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2005-1453

Affects:	leafnode versions 1.9.48 to 1.11.1 inclusively

Not affected:	leafnode 1.11.2

Default install: affected.

Corrected:	2005-05-04 10:09 UTC (CVS) - committed corrected version
		2005-05-04                   leafnode 1.11.2 released

0. Release history

2005-05-04	1.00 initial announcement
2005-05-07	1.01 add CVE name and URL

1. Background

leafnode is a store-and-forward proxy for Usenet news, is uses the
network news transfer protocol (NNTP). It consists of several
collaborating programs, the server part is usually started by inetd,
xinetd or tcpserver, the client part is usually started by cron or
manually.

This security announcement pertains to leafnode-1, the stable branch.

The leafnode-2 development branch has not yet seen a stable release, so
it is not subject to security announcements.

2. Problem description

Two vulnerabilities were found in the fetchnews program (the NNTP
client). These can cause the fetchnews program to crash when the
upstream server closes the connection while leafnode is receiving (1) an
article header, or (2) an article body.

3. Impact

A malicious upstream server that purposefully drops the connection after
fetchnews has requested an article header or body can prevent fetchnews
from ever querying other servers that are listed after the malicious
server in the configuration file.

4. Workaround

Comment out all configuration pertaining to the malicious server.

Note that this is not a full solution as transient network errors can
also cause delays in querying other network servers, and it requires
manual intervention to find out which server is malicious.

5. Solution

Upgrade your leafnode package to version 1.11.2.
leafnode 1.11.2 is available from SourceForge:
<http://sourceforge.net/project/showfiles.php?group_id=57767>

Leafnode 1.X versions are deemed stable, and it is usually best to go
for the latest released 1.X version to have all the other bug fixes as
well.

A. References

leafnode home page: <http://www.leafnode.org/>

END OF leafnode-SA-2005:01.fetchnews-crashes-on-timeout
