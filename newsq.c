/*
 * find out what is in your out.going or failed.postings directory
 *
 * Written by Cornelius Krasel <krasel@wpxx02.toxi.uni-wuerzburg.de>.
 * Copyright 1999.
 * Enhanced by Matthias Andree <matthias.andree@gmx.de>
 * Copyright 2002.
 *
 * See file COPYING for restrictions on the use of this software.
 */

#include "leafnode.h"
#include "mastring.h"

#include <sys/types.h>
#include <sys/stat.h>
#include "system.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int verbose = 0;
int debug = 0;

/* in-situ LF remover, returns pointer to modified string */
static char *droplf(char *t) {
    char *i, *j;

    if (t) {
	for (i = j = t; *i; i++) {
	    if (*i != '\n') *(j++) = *i;
	}
	*j = '\0';
    }
    return t;
}

/* in-situ NULL -> "(null)" mapper for strings */
static const char *catch0(const char *i) {
    return i ? i : "(null)";
}

int
main(const int argc, char **argv)
{
    FILE *f;
    DIR *d;
    struct dirent *de;
    struct stat st;
    unsigned long filesize;
    mastr *dirname = mastr_new(PATH_MAX);
    const char *s = mastr_str(dirname);
    int empty = 1;

    if (argc > 1 && argv[1] && 0 == strcasecmp(argv[1], "-f"))
	mastr_vcat(dirname, spooldir, "/failed.postings", NULL);
    else
	mastr_vcat(dirname, spooldir, "/out.going", NULL);

    if (chdir(s) < 0) {
	fprintf(stderr, "Cannot change to %s -- aborting.\n", s);
	exit(1);
    }
    d = opendir(".");
    if (!d) {
	fprintf(stderr, "Cannot open directory %s -- aborting.\n", s);
	exit(1);
    }

    printf("Contents of queue in directory %s:\n", s);
    while ((de = readdir(d))) {
	if (stat(de->d_name, &st)) {
	    fprintf(stderr, "Cannot stat %s\n", de->d_name);
	} else if (S_ISREG(st.st_mode)) {
	    f = fopen(de->d_name, "r");
	    if (f) {
		char *t1, *t2, *t3, *t4;
		
		empty = 0;
		filesize = st.st_size;
		printf("%s: %s %8lu bytes, spooled %s"
		       "\tFrom:       %-.66s\n"
		       "\tNewsgroups: %-.66s\n"
		       "\tSubject:    %-.66s\n"
		       "\tMessage-ID: %-.66s\n",
		       de->d_name,
		       st.st_mode & S_IRUSR ? "complete  " : "INCOMPLETE",
		       filesize, ctime(&st.st_mtime),
		       catch0(droplf(t1 = fgetheader(f, "From:"))),
		       catch0(droplf(t2 = fgetheader(f, "Newsgroups:"))),
		       catch0(droplf(t3 = fgetheader(f, "Subject:"))),
		       catch0(droplf(t4 = fgetheader(f, "Message-ID:"))));
		if (t1) free(t1);
		if (t2) free(t2);
		if (t3) free(t3);
		if (t4) free(t4);
		fclose(f);
	    } else
		fprintf(stderr, "Cannot open %s\n", de->d_name);
	}
    }
    closedir(d);
    if (empty) puts("The queue is empty.");
    mastr_delete(dirname);
    exit(0);
}
